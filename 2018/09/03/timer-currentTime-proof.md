# Thoughts about proving that a monitor's server and packet timers will never have undefined currentTime inside the check function:

Statement:
  A monitor's packet timer will never have undefined currentTime inside the check function:

Analysis:
  Given that:
  * The packet timer is ticked using the timestamp of the last sample applied to the entity being monitored
  * All samples of a packet are applied to an entity before the corresponding call to check
  
    Therefore, we really just have to prove that there is at least one sample applied to the entity before every check is triggered
      * Check is triggered via 3 ways (one way is via packet timer expiry which this proof is about):
        1) sample-triggered <- (sample for the entity)
        2) server-timer-timeout-triggered <- (sample for any entity with timestamp >= expiryTime)
            * This will cause check to be called with the monitor's packet time as undefined if the server timer expiry is caused by a sample for another entity while the packet timer's currentTime is undefined. // TODO think of ways to prevent this from happening, probably not needed for now (as server timers are not used yet) and I think we can implement this later on without having to rewrite stuff.

Statement:
  A monitor host's server timer will never have undefined currentTime inside the check function:

Analysis:
  Given that:
  * The server timer is ticked using the timestamp of a Server state sample.
  * A Server state sample is emitted by every packet that enters the system.

    Therefore, we just have to prove that there is at least one packet that comes into the system before every check is triggered:
      * check is triggered via 3 ways (one way is via server timer expiry which this proof is about):
        1) sample-triggered <- (sample for the entity) <- (packet that contains at least 1 sample for the entity)
        2) packet-timer-timeout-triggered <- (sample for the entity with timestamp >= expiryTime) <-  (packet that contains at least 1 sample for the entity)

    Points 1) and 2) happening implies that there would be at least 1 packet that comes through to the system