* Motivation is weak and short-sighted, you need discipline.
* Everyone must choose __ONE__ of __TWO__ pains: _the pain of discipline_ or the _pain of regret_