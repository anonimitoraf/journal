* Before attempting to design interfaces, you first have to figure out where the interfaces are going to be located.
* Importance of uptime and load handled by a service. How will the system be if this service went down?
* Be mindful of breaking changes. Even if a spec does not explicitly go over each and every detail of the system, maybe think about any implicit assumptions that users could have made that will now break because of an update to your lib, package, etc.
* State constraints that will be convenient for users and make sure you hold them. 
  * e.g. In the live monad library, the constraint of subscriber.initial only ever being called once (unless an error occurs in between 2 subscriber.initial calls). This constraint would not have been held if observer.initial calls were possible to be called twice without an error being thrown. How Michael fixed this, is the observer.value is called and that either triggered subscriber.initial or subscriber.change depending on the subscriber state.
* Caching: Assess how much complexity will be added due to caching and ask yourself: "Is it worth it?". Caching might even just be implemented external from the systems.
* Attempt to define the smallest set of states possible because in theory, you can define a system to have an infinite number of states (e.g. A producer/consumer queue can be defined with 2 states: Producer vs Consumer triggering the processing )
* Promise-chaining: a way to easily implement async queues
* Think about whether or not a callback might be called asynchronously or not. If so, think about what would happen if it is called unexpectedly in an instant.
* Plan for Regression Testing at the very start because, trust me, it's very worth it to always have Regression Tests up
* Polling might be a good if a queue is expected to be almost always populated

* Always think about the constraints/invariants (e.g. atomicity, sequencing, etc) that your code will impose to the other pieces that might use your code