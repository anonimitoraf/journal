* Using synchronization tokens as a way to make sure that 2 events happen before proceeding. This can be done by having a shared flag (sort of like a semaphore) that need to receive both event-notifications from the 2 events. 
* How to spot race conditions:
  * If the input is ordered, and the output is also expected to be ordered, is there any way this piece of code break the latter statement?
  * Promise-chaining is a way to avoid race conditions (e.g. sequencing async change callbacks in couch)